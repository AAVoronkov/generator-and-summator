import numpy as np
import os
import matplotlib.pyplot as plt

file_path = os.path.join(os.getcwd(), 'files')

try:
    # Считываем пути к файлам
    with open('list.txt', 'r') as f:
        files = f.read().split('\n')

except FileNotFoundError:
    print('There are no files to work with!')

else:

    array_total = []

    # Формируем массив чисел из всех файлов
    for i in range(0, len(files) - 1):

        try:
            file_name = files[i].split('\\')[-1]
            full_file_path = os.path.join(file_path, file_name)

            numbers = np.loadtxt(full_file_path, dtype=np.int32)
            array_total.append(numbers)

        # Пропускаем несуществующие файлы
        except OSError:
            pass

    array_total = np.asarray(array_total)

    # Если в массиве нет ни одного элемента, то завершаем программу
    if array_total.shape == (0,):
        print('There are no files to work with!')

    else:
        # Считываем и сохраняем в файл средние значения элементов
        mean = np.mean(array_total, axis=0)
        np.savetxt('mean.txt', mean, newline=' ', fmt='%d')

        # Код для построения графика
        x = np.arange(1, array_total.shape[1] + 1, 1)
        plt.plot(x, mean)
        plt.show()

        print('Done!')


def test_sum():
    """
    Небольшая тест-функция.
    1 тест - переменная со средними элементами является массивом.
    2 тест - средние значения лежат в пределах от 0 до 1.000.000.
    3 тест - количество средних значений равно количеству чисел в файлах.
    """
    print("Test №1:", "Ok" if type(mean) == np.ndarray else "Fail")
    print("Test №2:", "Ok" if (mean.all() >= 0 or mean.all() < 1000000) else "Fail")
    print("Test №3:", "Ok" if mean.shape[0] == array_total.shape[1] else "Fail")


# Запустить тест-функцию
# test_sum()
