import numpy as np
import os
from pathlib import Path
import time

# Создаём каталог files для хранения файлов
Path('./files').mkdir(parents=True, exist_ok=True)


def create_files(count):
    """
    Функция принимает аргументом количество файлов для создания.
    В качестве имени создаваемых файлов используется timestamp.
    В файл записываются числа от 0 до 1.000.000 невключительно.
    """
    for i in range(0, count):
        array = np.random.randint(0, 1000000, size=1000, dtype=np.int32)

        file_name = ''.join(str(time.time()).split('.')) + '.txt'
        full_file_path = os.path.join(os.getcwd(), 'files', file_name)
        np.savetxt(full_file_path, array, fmt='%d')

        # Дозаписываем путь к новому файлу в list.txt
        with open('list.txt', 'a') as f:
            f.write(full_file_path + '\n')

    print('Files created: {}'.format(count))


# Создать 1 файл
create_files(1)
# Создать 2000 файлов
create_files(2000)
